﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_2_7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Task 1
            Console.WriteLine("Task 1");
            string fullName = "Arkhipov Yuriy Sergeevich";
            byte age = 35;
            string email = "omadonex@yandex.ru";            
            float scoreProg = 86.5f;
            float scoreMath = 99.3f;
            float scorePhys = 77.4f;

            string pattern = "Full Name: {0}\nAge: {1}\nE-mail: {2}\nScore: programming: {3} math: {4} physics: {5}";
            Console.WriteLine(pattern, fullName, age, email, scoreProg, scoreMath, scorePhys);

            Console.ReadKey(false);

            //Task 2
            Console.WriteLine("Task 2");
            float sumScore = scoreProg + scoreMath + scorePhys;
            float averageScore = sumScore / 3;
            Console.WriteLine("Total Score: {0}\nAverage score: {1}", sumScore, averageScore);

            Console.ReadKey(false);
        }
    }
}
